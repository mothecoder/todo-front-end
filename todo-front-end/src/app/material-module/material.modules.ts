import { NgModule } from '@angular/core';
import {MdcButtonModule} from '@angular-mdc/web/button';
import {MdcCardModule, MdcCard} from '@angular-mdc/web/card';
import {MdcSnackbarModule} from '@angular-mdc/web/snackbar';
import {MdcTopAppBarModule} from '@angular-mdc/web/top-app-bar';
import {MdcIconModule} from '@angular-mdc/web/icon';
import {MdcDialogModule} from '@angular-mdc/web/dialog';
import {MdcFormFieldModule} from '@angular-mdc/web/form-field';
import {MdcTextFieldModule} from '@angular-mdc/web/textfield';
import {MdcLinearProgressModule} from '@angular-mdc/web/linear-progress';


@NgModule({
  exports: [
        MdcButtonModule,
        MdcCardModule,
        MdcSnackbarModule,
        MdcTopAppBarModule,
        MdcIconModule,
        MdcDialogModule,
        MdcFormFieldModule,
        MdcTextFieldModule,
        MdcLinearProgressModule,
  ],
})
export class MaterialModule { }
