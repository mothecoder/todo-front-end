import { AddTodoComponent } from './../home/dialogs/add-todo/add-todo.component';
import { Component, OnInit } from '@angular/core';
import { MdcDialog, MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(public dialog: MdcDialog) { 

  }

  ngOnInit(): void {
  }

  addItemDialog(){
  const dialogRef = this.dialog.open(AddTodoComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: 'my-dialog',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}
