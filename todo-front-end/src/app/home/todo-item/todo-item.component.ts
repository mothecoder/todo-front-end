import { fade, slideInOutAnimation, fadeInAnimation } from './../../other/animation';
import { AddTodoComponent } from './../dialogs/add-todo/add-todo.component';
import { AppStateService } from './../../app-state/loader/app-state.service';
import { TodoItemService } from './../service/todo-item.service';
import { AssetManagementService } from './../service/asset-management.service';
import { TodoItem } from './../../../models/todo-item';
import { Component, OnInit, Input } from '@angular/core';
import { MdcDialog } from '@angular-mdc/web';
import { transition, state, style, trigger, animate , query, stagger, keyframes} from '@angular/animations';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent implements OnInit {
  @Input() data: TodoItem;

  constructor(
    private assetManagementService: AssetManagementService, 
    private todoService: TodoItemService, 
    private appStateService: AppStateService,      
    public dialog: MdcDialog,
    ) { 

  }

  ngOnInit(): void {
  
  }
  remove(){

    this.appStateService.loadingOn();
      this.todoService.deleteItem(this.data.id).subscribe(result=>{
        this.assetManagementService.removeTodoItem(this.data.id);
      });
  }
  update(){
    this.dialog.open(AddTodoComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: 'my-dialog',
      data: this.data
    });

  }
}
