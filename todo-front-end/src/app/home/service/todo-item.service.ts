import { AppStateService } from './../../app-state/loader/app-state.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,  } from 'rxjs';
import { finalize  } from 'rxjs/operators';

import { TodoItem } from 'src/models/todo-item';

@Injectable({
  providedIn: 'root'
})
export class TodoItemService {



  constructor(private http: HttpClient, private appStateService: AppStateService) { 

  }

  getAllItems():Observable<TodoItem[]>{
    return this.http.get<TodoItem[]>('http://localhost:8082/todo/get-items').pipe(
      finalize(()=> this.appStateService.loadingOff())
    );
  }

  getItemsByUserId(userId: string){
    return this.http.get<TodoItem[]>(`http://localhost:8082/todo/find-by-user-id/?userId=${userId}`).pipe(
      finalize(()=> this.appStateService.loadingOff())
    );
  }

  deleteItem(id: string){
    return this.http.get(`http://localhost:8082/todo/delete-item/?id=${id}`).pipe(
      finalize(()=> this.appStateService.loadingOff())
    );
  }


  saveItem(item:TodoItem){
    return this.http.post(`http://localhost:8082/todo/save-item`,item).pipe(
      finalize(()=> this.appStateService.loadingOff())
    );
  }
  

}
