import { TodoItem } from 'src/models/todo-item';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AssetManagementService {
  todoItems: TodoItem[] = [

  ]
  private _notification:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public notification:Observable<boolean> = this._notification.asObservable();
  constructor() { }

  getTodoItems(){
    return this.todoItems;
  }
  setTodoItems(todoItems: TodoItem[]){
    this.todoItems = todoItems;
  }
  removeTodoItem(id: String){
    console.log('Before' , this.todoItems)
    this.todoItems =  this.todoItems.filter(item=>item.id !== id);
    console.log('After' , this.todoItems)
    this._notification.next(true);
  }
  addTodoItem(item:TodoItem){
    this.todoItems.push(item);
    this._notification.next(true);
  }
  updateTodoItem(item:TodoItem){
    let index:number = this.todoItems.findIndex((e => e.id === item.id));
    this.todoItems[index] = item;
    this._notification.next(true);
  }
}
