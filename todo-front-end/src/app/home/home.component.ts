import { fade, flyInOut, cardAnimation } from './../other/animation';
import { AppStateService } from './../app-state/loader/app-state.service';
import { AssetManagementService } from './service/asset-management.service';
import { TodoItemService } from './service/todo-item.service';
import { TodoItem } from './../../models/todo-item';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations:[
    cardAnimation  
  ]
})
export class HomeComponent implements OnInit {

  items:TodoItem[]=[

  ]
  constructor(private todoService: TodoItemService, private assetManagementService: AssetManagementService, private appStateService: AppStateService) { }

  ngOnInit(): void {
    this.appStateService.loadingOn();
    this.todoService.getAllItems().subscribe((result:TodoItem[])=>{
      console.log('Got the result');
      this.assetManagementService.setTodoItems(result);
       this.items = this.assetManagementService.getTodoItems();
    })

    this.assetManagementService.notification.subscribe((event)=>{
      this.items = this.assetManagementService.getTodoItems();
    })
  }

}
