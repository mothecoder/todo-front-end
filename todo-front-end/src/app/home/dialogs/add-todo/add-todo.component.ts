import { AppStateService } from './../../../app-state/loader/app-state.service';
import { AssetManagementService } from './../../service/asset-management.service';
import { TodoItem } from 'src/models/todo-item';
import { TodoItemService } from './../../service/todo-item.service';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {

  profileForm:FormGroup;
  constructor(
    public dialogRef: MdcDialogRef<AddTodoComponent>, 
     private formBuilder: FormBuilder,
     private todoItemService: TodoItemService,
     private assetManagementService: AssetManagementService,
     private appStateService: AppStateService,
     @Inject(MDC_DIALOG_DATA) public data: TodoItem
     ) { 
    // this.profileForm = new FormGroup({
    //   title: new FormControl('Sample Title', Validators.required),
    //   description: new FormControl('Sample Description', Validators.required),
    // });
    console.log('Value of passed data is', this.data)
    this.profileForm = this.formBuilder.group({
      title:['Sample Title', Validators.required],
      description:['Sample Description', Validators.required]
    })
    if(this.data){
    this.profileForm.patchValue({
      title: data.title,
      description: data.description
    });
    }

  }

  ngOnInit(): void {

  }
  submit(): void {
    if (this.profileForm.invalid) {
      return;
    }
    let item:TodoItem = {
      description:this.profileForm.get('description').value,
      title: this.profileForm.get('title').value,
      userId: 'Mo'
    }
    if(this.data){
      item.id = this.data.id;
      item.userId = this.data.userId;
    }
    this.appStateService.loadingOn();
    this.todoItemService.saveItem(item).subscribe(result=>{
      console.log('Result', result)
      if(this.data){
        this.assetManagementService.updateTodoItem(item);    
      }else{
        this.assetManagementService.addTodoItem(item);    
      }
    });
     this.dialogRef.close();

  }
}
