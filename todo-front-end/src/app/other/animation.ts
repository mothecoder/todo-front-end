import { trigger, state, style, transition, animate, stagger, keyframes, query } from "@angular/animations";

export let fade = trigger('fade',[
    state('void', style({opacity:0})),
    transition(':enter, :leave', [
      animate(1000)
    ]),
])
export let test = trigger('ok',[

    state('open', style({
        height:'200px',
        opacity:1,
        backgroundColor:'yellow'
    })),
    state('closed', style({
        height:'100px',
        opacity:0.5,
        backgroundColor:'green'
    }))
])
export let cardAnimation = [
      // Trigger animation cards array
      trigger('cardAnimation', [
        // Transition from any state to any state
        transition('* => *', [
          // Initially the all cards are not visible
          query(':enter', style({ opacity: 0 }), { optional: true }),
  
          // Each card will appear sequentially with the delay of 300ms
          query(':enter', stagger('300ms', [
            animate('.5s ease-in', keyframes([
              style({ opacity: 0, transform: 'translateY(-50%)', offset: 0 }),
              style({ opacity: .5, transform: 'translateY(-10px) scale(1.1)', offset: 0.3 }),
              style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
            ]))]), { optional: true }),
  
          // Cards will disappear sequentially with the delay of 300ms
          query(':leave', stagger('300ms', [
            animate('500ms ease-out', keyframes([
              style({ opacity: 1, transform: 'scale(1.1)', offset: 0 }),
              style({ opacity: .5, transform: 'scale(.5)', offset: 0.3 }),
              style({ opacity: 0, transform: 'scale(0)', offset: 1 }),
            ]))]), { optional: true })
        ]),
      ]),
]

export let plusAnimation = [
    trigger('plusAnimation', [

        // Transition from any state to any state
        transition('* => *', [
          query('.plus-card', style({ opacity: 0, transform: 'translateY(-40px)' })),
          query('.plus-card', stagger('500ms', [
            animate('300ms 1.1s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
          ])),
        ])
      ])
]

export let flyInOut = [
    trigger('flyInOut', [
        state('in', style({ transform: 'translateX(0)' })),
        transition('void => *', [
          style({ transform: 'translateX(-100%)' }),
          animate(100)
        ]),
        transition('* => void', [
          animate(200, style({ transform: 'translateX(100%)' }))
        ])
      ])
]
export const slideInOutAnimation =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('slideInOutAnimation', [

        // end state styles for route container (host)
        state('*', style({
            // the view covers the whole screen with a semi tranparent background
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.8)'
        })),

        // route 'enter' transition
        transition(':enter', [

            // styles at start of transition
            style({
                // start with the content positioned off the right of the screen, 
                // -400% is required instead of -100% because the negative position adds to the width of the element
                right: '-400%',

                // start with background opacity set to 0 (invisible)
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }),

            // animation and styles at end of transition
            animate('2.5s ease-in-out', style({
                // transition the right position to 0 which slides the content into view
                right: 0,

                // transition the background opacity to 0.8 to fade it in
                backgroundColor: 'rgba(0, 0, 0, 0.8)'
            }))
        ]),

        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            animate('2.5s ease-in-out', style({
                // transition the right position to -400% which slides the content out of view
                right: '-400%',

                // transition the background opacity to 0 to fade it out
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }))
        ])
    ]);

    export const fadeInAnimation =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('fadeInAnimation', [

        // route 'enter' transition
        transition(':enter', [

            // css styles at start of transition
            style({ opacity: 0 }),

            // animation and styles at end of transition
            animate('.3s', style({ opacity: 1 }))
        ]),
    ]);