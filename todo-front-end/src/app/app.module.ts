import { MaterialModule } from './material-module/material.modules';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { TodoItemComponent } from './home/todo-item/todo-item.component';
import { HttpClientModule } from '@angular/common/http';
import { AddTodoComponent } from './home/dialogs/add-todo/add-todo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationComponent } from './home/dialogs/confirmation/confirmation.component';
import { LoaderComponent } from './app-state/loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    TodoItemComponent,
    AddTodoComponent,
    ConfirmationComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
