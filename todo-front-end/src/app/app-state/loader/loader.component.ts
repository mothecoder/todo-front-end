import { AppStateService } from './app-state.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {


  state: Boolean = false;
  constructor(private appStateService: AppStateService) { }

  ngOnInit(): void {
   this.appStateService.appState.subscribe((state)=>{
    console.log('State changed', state)
    this.state = state
   });
  }

}
