import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppStateService {

  _appState: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  public appState: Observable<Boolean> = this._appState.asObservable();
  constructor() { 

  }
  loadingOn(){
      this._appState.next(true);
  }
  loadingOff(){
    this._appState.next(false);
  }
}
